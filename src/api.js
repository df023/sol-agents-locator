function normalize(agent) {
  return {
    phone: agent.string,
    whatsapp: agent.whatsapp,
    lat: +agent.lat,
    lng: +agent.lon,
    distance: agent.distance,
    workTimeStart: agent.work_time_begin,
    workTimeEnd: agent.work_time_end,
    address: agent.address,
    firstName: agent.first_name,
    lastName: agent.last_name,
    rating: agent.rating,
  };
}

export async function getAgentList() {
  const resp = await fetch(
    `${process.env.REACT_APP_BASE_URL}/agent/web-allocator-agent-list`,
    {
      method: "GET",
      headers: {
        "X-Client-Type": "android",
        "X-Client-Version": "181",
      },
    }
  );

  const respBody = await resp.json();

  if (!resp.ok) {
    throw new Error(respBody.request_error.message);
  }

  return respBody.result.list.map((agent) => normalize(agent));
}
