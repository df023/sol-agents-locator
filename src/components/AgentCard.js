import React from "react";

import Btn from "components/Btn";

import { ReactComponent as StarIcon } from "assets/img/star.svg";
import { ReactComponent as MapIcon } from "assets/img/map.svg";

export default function AgentCard({
  data: { firstName, lastName, phone, whatsapp, address, distance, rating },
  isMapAvailable,
  showOnMap,
}) {
  return (
    <li className="flex flex-col bg-blue-200 text-gray-200 p-12 rounded-10">
      <p className="flex items-center mb-8">
        <span className="text-white-100 text-18 leading-20 tracking-tight mr-8">
          {firstName} {lastName}
        </span>
        <StarIcon className="mr-4 w-20 h-20" />
        {rating}
      </p>
      <p className="mb-12">
        {address}
        <span className="text-white-100"> ({distance} km)</span>
      </p>
      <div className="flex justify-between">
        <div className="flex">
          <Btn href={`tel:${phone}`} className="min-w-98 mr-12">
            Call me
          </Btn>
          <Btn
            href={`https://api.whatsapp.com/send?phone=${whatsapp}`}
            className="min-w-98"
          >
            WhatsApp
          </Btn>
        </div>
        <Btn type="primary" disabled={!isMapAvailable} onClick={showOnMap}>
          <MapIcon className="w-20 h-20" />
        </Btn>
      </div>
    </li>
  );
}
