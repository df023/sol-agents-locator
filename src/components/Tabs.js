import React from "react";
import classnames from "classnames";

export default function Tabs({ tabs, activeTab, changeTab }) {
  const tabWidth = Math.round((100 / tabs.length) * 100) / 100;

  return (
    <div className="text-16 text-gray-200 leading-20">
      {tabs.map((tab, index) => (
        <button
          key={index}
          className={classnames({
            "p-14 font-medium select-none": true,
            "text-white-100": index === activeTab,
          })}
          style={{ width: `${tabWidth}%` }}
          onClick={() => changeTab(index)}
        >
          {tab}
        </button>
      ))}

      <span
        className="tab-highlight"
        style={{
          width: `${tabWidth}%`,
          transform: `translateX(${tabWidth * activeTab * tabs.length}%)`,
        }}
        role="presentation"
      />
    </div>
  );
}
