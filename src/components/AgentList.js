import React from "react";
import classnames from "classnames";

import AgentCard from "./AgentCard";

export default function AgentList({
  className,
  isMapAvailable,
  showOnMap,
  list,
}) {
  return (
    <ul className={classnames(["px-16 pb-16 space-y-8", className])}>
      {list.map((agent, index) => (
        <AgentCard
          data={agent}
          isMapAvailable={isMapAvailable}
          showOnMap={() => {
            showOnMap(index);
          }}
          key={index}
        />
      ))}
    </ul>
  );
}
