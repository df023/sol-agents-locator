import React from "react";
import classnames from "classnames";
import Btn from "components/Btn";

import { isWorkingHours, isSameLocation } from "helpers";

import { ReactComponent as StarIcon } from "assets/img/star.svg";
import { ReactComponent as RouteIcon } from "assets/img/route.svg";
// import { ReactComponent as DragIcon } from "assets/img/drag.svg";
import { ReactComponent as PlusIcon } from "assets/img/plus.svg";

export default function MapAgentInfo({
  agentData: {
    firstName,
    lastName,
    rating,
    address,
    distance,
    lat,
    lng,
    phone,
    whatsapp,
    workTimeStart,
    workTimeEnd,
  },
  destination,
  showDirections,
  constructRoute,
}) {
  const isWorking = isWorkingHours(workTimeStart, workTimeEnd);
  const isRouteConstructed =
    isSameLocation({ lat, lng }, destination) && showDirections;
  const Icon = isRouteConstructed ? PlusIcon : RouteIcon;

  return (
    <div className="absolute z-30 left-0 right-0 bottom-0 flex flex-col bg-white-100 text-gray-200 rounded-t-18 px-16 pt-14 pb-34">
      {/*<DragIcon className="self-center mb-18" />*/}
      <div className="flex items-center justify-between mb-6">
        <p className="flex items-center">
          <span className="text-black text-18 leading-20 tracking-tight mr-8">
            {firstName} {lastName}
          </span>
          <StarIcon className="mr-4 w-20 h-20" />
          {rating}
      </p>
        <p className="font-medium"> ({distance} km)</p>
      </div>
      <p className={classnames({ "mb-12": true, "text-green": isWorking })}>
        {isWorking
          ? "Now works"
          : `Working hours: ${workTimeStart} - ${workTimeEnd}`}
      </p>
      <p className="flex justify-between mb-16">
        {address}
        <button
          className="flex-shrink-0 flex justify-center items-center w-40 h-40 bg-yellow text-black rounded-full"
          onClick={constructRoute}
        >
          <Icon
            className={classnames({
              "w-24 h-24": true,
              "transform rotate-45": isRouteConstructed,
            })}
          />
        </button>
      </p>
      <div className="flex">
        <Btn type="gray" href={`tel:${phone}`} className="min-w-98 mr-12 w-1/2">
          Call me
        </Btn>
        <Btn
          type="gray"
          href={`https://api.whatsapp.com/send?phone=${whatsapp}`}
          className="min-w-98 w-1/2"
        >
          WhatsApp
        </Btn>
      </div>
    </div>
  );
}
