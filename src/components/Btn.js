import React from "react";
import classnames from "classnames";

export default function Btn({
  type = "secondary",
  className,
  children,
  ...props
}) {
  const classes = [
    "flex justify-center items-center rounded-8 text-14 leading-18 font-medium px-12",
    className,
  ];

  const Element = props.href ? "a" : "button";

  switch (type) {
    case "gray":
      classes.push("text-black bg-gray-300 py-11");
      break;
    case "secondary":
      classes.push("text-white-100 bg-blue-100 py-5");
      break;
    case "primary":
    default:
      classes.push("text-black bg-yellow py-5");
  }

  return (
    <Element className={classnames(classes)} {...props}>
      {children}
    </Element>
  );
}
