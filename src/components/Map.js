import React, { useContext, useState, useEffect, useMemo } from "react";
import classnames from "classnames";
import {
  GoogleMap,
  Marker,
  DirectionsService,
  DirectionsRenderer,
  useLoadScript,
} from "@react-google-maps/api";
import { MapContext } from "context";
import { isSameLocation } from "helpers";

import markerInactiveIcon from "assets/img/marker-inactive.svg";
import markerActiveIcon from "assets/img/marker-active.svg";
// import markerUserLocIcon from "assets/img/marker-userloc.svg";

import { ReactComponent as LoaderIcon } from "assets/img/loader.svg";
import { ReactComponent as MinusIcon } from "assets/img/minus.svg";
import { ReactComponent as PlusIcon } from "assets/img/plus.svg";
import { ReactComponent as NavigationIcon } from "assets/img/navigation-arrow.svg";

import MapAgentInfo from "./MapAgentInfo";

export default React.memo(function Map({
  agents,
  className,
  selectedAgentIndex,
  center,
  changeSelected,
  ...props
}) {
  const selectedAgent =
    selectedAgentIndex !== null ? agents[selectedAgentIndex] : null;

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_MAPS_TOKEN,
  });
  const [userLocation, setUserLocation] = useState(null);
  const { map, setMap } = useContext(MapContext);
  const [destination, setDestination] = useState(null);
  const [directions, setDirections] = useState(null);
  const [showDirections, setShowDirections] = useState(false);

  function directionsCallback(resp) {
    if (resp !== null) {
      if (resp.status === "OK") {
        setDirections(resp);
      } else {
        console.log("response: ", resp);
      }
    }
  }

  function constructRoute() {
    if (isSameLocation(selectedAgent, destination)) {
      setShowDirections(!showDirections);
    } else {
      setShowDirections(true);
      setDestination({ lat: selectedAgent.lat, lng: selectedAgent.lng });
    }
  }

  const mapDirections = useMemo(() => {
    return (
      <>
        {destination && (
          <DirectionsService
            options={{
              destination,
              origin: userLocation,
              travelMode: "DRIVING",
            }}
            callback={directionsCallback}
          />
        )}

        {showDirections && directions && (
          <DirectionsRenderer options={{ directions, suppressMarkers: true }} />
        )}
      </>
    );
  }, [destination, directions, showDirections, userLocation]);

  useEffect(() => {
    const watchID = navigator.geolocation.watchPosition(
      ({ coords }) => {
        setUserLocation({
          lat: coords.latitude,
          lng: coords.longitude,
          heading: coords.heading || 0,
        });
      },
      undefined,
      {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000,
      }
    );

    return () => {
      navigator.geolocation.clearWatch(watchID);
    };
  }, []);

  let mapContent;

  if (!navigator.geolocation) {
    mapContent = <p>Can't get user location</p>;
  } else if (loadError) {
    mapContent = <p>Sorry, map cannot be loaded right now</p>;
  } else if (!(userLocation && isLoaded)) {
    mapContent = (
      <div className="h-full flex flex-col justify-center text-24 leading-28 text-white-100 items-center">
        <p className="mb-24">Loading</p>
        <LoaderIcon className="w-70 h-70" />
      </div>
    );
  } else {
    mapContent = (
      <GoogleMap
        {...props}
        mapContainerClassName="w-full h-full"
        zoom={10}
        options={{
          disableDefaultUI: true,
          gestureHandling: "greedy",
        }}
        onClick={() => changeSelected(null)}
        onLoad={(map) => {
          map.setCenter(userLocation);
          setMap(map);
        }}
      >
        {mapDirections}
        <Marker
          position={userLocation}
          icon={{
            path: window.google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            rotation: userLocation.heading,
            scale: 4,
            strokeColor: "#021318",
            strokeWeight: 3,
          }}
          onClick={() => {
            map.panTo(userLocation);
          }}
        />
        {agents.map(({ lat, lng }, index) => (
          <Marker
            position={{ lat, lng }}
            icon={
              selectedAgentIndex === index
                ? markerActiveIcon
                : markerInactiveIcon
            }
            onClick={() => {
              const { lat, lng } = agents[index];
              map.panTo({ lat, lng });
              changeSelected(index);
            }}
            key={index}
          />
        ))}
        {selectedAgentIndex !== null && (
          <MapAgentInfo
            agentData={selectedAgent}
            destination={destination}
            showDirections={showDirections}
            constructRoute={constructRoute}
          />
        )}
        <div className="absolute z-20 top-0 bottom-0 text-black flex flex-col justify-center right-0 mr-16 pointer-events-none">
          <button
            className="flex justify-center items-center w-40 h-40 bg-yellow rounded-full mb-18 mt-auto pointer-events-auto"
            onClick={() => map.setZoom(map.getZoom() + 1)}
          >
            <PlusIcon className="w-24 h-24" />
          </button>
          <button
            className="flex justify-center items-center w-40 h-40 bg-yellow rounded-full mb-18 pointer-events-auto"
            onClick={() => map.setZoom(map.getZoom() - 1)}
          >
            <MinusIcon className="w-24 h-24" />
          </button>
          <button
            className="flex justify-center items-center w-40 h-40 bg-yellow rounded-full mb-auto pointer-events-auto"
            onClick={() => map.panTo(userLocation)}
          >
            <NavigationIcon className="w-24 h-24" />
          </button>
        </div>
      </GoogleMap>
    );
  }

  return (
    <div
      className={classnames([
        "w-full h-full flex justify-center items-center",
        className,
      ])}
    >
      {mapContent}
    </div>
  );
});
