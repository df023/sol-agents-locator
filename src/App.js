import React, { useState, useEffect } from "react";
import classnames from "classnames";

import { getAgentList } from "api";
import { MapContext } from "context";

import Btn from "components/Btn";
import Tabs from "components/Tabs";
import AgentList from "components/AgentList";
import Map from "components/Map";

import { ReactComponent as LoaderIcon } from "assets/img/loader.svg";
// import { ReactComponent as ArrowIcon } from "assets/img/arrow.svg";
// import { ReactComponent as FilterIcon } from "assets/img/filter.svg";

const tabs = ["Show as a list", "Show on map"];

export default function App() {
  const [fetchError, setFetchError] = useState(null);
  const [activeTab, setActiveTab] = useState(1);
  const [map, setMap] = useState(null);
  const [solAgents, setSolAgents] = useState(null);
  const [selectedAgentIndex, setSelectedAgentIndex] = useState(null);

  function loadAgents() {
    getAgentList()
      .then((list) => setSolAgents(list))
      .catch((error) => setFetchError(error.message));
  }

  function showAgentOnMap(agentIndex) {
    setActiveTab(1);
    setSelectedAgentIndex(agentIndex);

    const { lat, lng } = solAgents[agentIndex];
    map.panTo({ lat, lng });
  }

  useEffect(loadAgents, []);

  let mainContent;

  if (fetchError) {
    mainContent = (
      <div className="h-full flex flex-col justify-center items-center">
        <p className="text-16 mb-18">Sorry, there was an error on the server</p>
        <Btn
          type="primary"
          onClick={() => {
            setFetchError(null);
            loadAgents();
          }}
        >
          Try again
        </Btn>
      </div>
    );
  } else if (solAgents) {
    mainContent = (
      <MapContext.Provider value={{ map, setMap }}>
        <AgentList
          list={solAgents}
          className={classnames({ hidden: activeTab !== 0 })}
          isMapAvailable={!!map}
          showOnMap={showAgentOnMap}
        />
        <Map
          agents={solAgents}
          selectedAgentIndex={selectedAgentIndex}
          changeSelected={setSelectedAgentIndex}
          className={classnames({ hidden: activeTab !== 1 })}
        />
      </MapContext.Provider>
    );
  } else {
    mainContent = (
      <div className="h-full flex flex-col justify-center text-24 leading-28 text-white-100 items-center">
        <p className="mb-24">Loading</p>
        <LoaderIcon className="w-70 h-70" />
      </div>
    );
  }

  return (
    <>
      <header className="px-16 pb-16">
        <div className="relative py-18">
          {/*<button className="absolute top-0 bottom-0 my-auto w-20 h-20">*/}
          {/*  <ArrowIcon className="text-yellow w-16 h-16" />*/}
          {/*</button>*/}
          <p className="text-18 leading-20 text-center font-medium">
            Agent Locator
          </p>
          {/*<button className="absolute right-0 top-0 bottom-0 my-auto w-20 h-20">*/}
          {/*  <FilterIcon className="text-yellow w-20 h-20" />*/}
          {/*</button>*/}
        </div>
        <Tabs tabs={tabs} activeTab={activeTab} changeTab={setActiveTab} />
      </header>
      <main className="content">{mainContent}</main>
    </>
  );
}
