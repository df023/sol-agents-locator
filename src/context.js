import { createContext } from "react";

export const MapContext = createContext(null);
MapContext.displayName = "MapContext";
