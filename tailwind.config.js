module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
  },
  purge: ["./src/**/*.html", "./src/**/*.js"],
  theme: {
    colors: {
      blue: {
        100: "#162B32",
        200: "#0A2027",
      },
      green: "#42EA60",
      yellow: "#FFF700",
      white: {
        100: "#FFFFFF",
        200: "#ECECEC",
      },
      black: "#021318",
      gray: {
        100: "#D2D2D2",
        200: "#7C9299",
        300: "#EEEEEE",
      },
    },
    fontSize: {
      13: "1.3rem",
      14: "1.4rem",
      16: "1.6rem",
      18: "1.8rem",
      24: "2.4rem",
    },
    lineHeight: {
      16: "1.6rem",
      18: "1.8rem",
      20: "2rem",
      28: "2.8rem",
    },
    letterSpacing: {
      none: "0",
      normal: "0.02rem",
      tight: "0.01rem",
    },
    spacing: {
      0: "0",
      2: "0.2rem",
      4: "0.4rem",
      5: "0.5rem",
      6: "0.6rem",
      8: "0.8rem",
      11: "1.1rem",
      12: "1.2rem",
      14: "1.4rem",
      16: "1.6rem",
      18: "1.8rem",
      20: "2rem",
      24: "2.4rem",
      28: "2.8rem",
      34: "3.4rem",
      40: "4rem",
      70: "7rem",
    },
    borderRadius: {
      full: "999px",
      8: "0.8rem",
      10: "1rem",
      18: "1.8rem",
    },
    minWidth: {
      98: "9.8rem",
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
